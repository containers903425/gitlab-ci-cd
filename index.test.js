const app = require("./index.js");

sp = require("supertest");


describe("GET /", () => {
  it("Verify the response of the api Hello World", async () => {
    const response = await sp(app).get("/");
    expect(response.statusCode).toBe(200);
    expect(response.text).toEqual("Hello World");
  });
});

beforeAll((done)=>{
  app.server.close()
  done()
})