const express = require('express')
const app = express();

app.get('/', (req, res) => {
    res.send('Hello World');
});

const port = 3000;
var server = app.listen(port, () => {
    console.log(`Server is running on http://localhost:3000`);
});

app.server = server;

module.exports = app;
